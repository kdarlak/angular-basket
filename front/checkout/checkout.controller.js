'use strict';

angular.module('basket').controller('CheckoutController', function($scope, $resource) {

  $scope.discount = null;
  $scope.card = {};
  $scope.code = {
    number: ''
  };

  var codesResource = $resource('http://localhost:9001/promocode');

  $scope.checkPromoCode = function() {
    var data = { promoCode: $scope.code.value };
    codesResource.save(data, function(resp) {
      $scope.discount = resp;
      alert('Valid promo code!');
    }, function(resp) {
      $scope.discount = null;
    });
  }

  var checkoutResource = $resource('http://localhost:9001/checkout');

  $scope.order = function() {
    var data = {
      basket: $scope.basket.products,
      cardNumber: $scope.card.number
    };
    checkoutResource.save(data, function(resp) {
      alert('You order successfully!');
    }, function(resp) {
      alert(resp.data.errors[0].msg);
    });
  }

});
