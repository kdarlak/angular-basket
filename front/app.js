'use strict';

angular.module('basket', ['ngResource', 'underscore', 'ui.router']);

angular.module('basket').config(function($stateProvider, $urlRouterProvider) {

  var mainState = {
    name: 'app',
    url: '/',
    abstract: true,
    views: {
      "layout": {
        templateUrl: 'layout.html',
        controller: 'MainController'
      }
    }
  }

  var listState = {
    name: 'app.list',
    url: 'list',
    views: {
      "main": {
        templateUrl: 'list/list.html',
        controller: 'ListController'
      }
    }
  }

  var checkoutState = {
    name: 'app.checkout',
    url: 'checkout',
    views: {
      "main": {
        templateUrl: 'checkout/checkout.html',
        controller: 'CheckoutController'
      }
    }
  }

  $stateProvider.state(mainState);
  $stateProvider.state(listState);
  $stateProvider.state(checkoutState);
  $urlRouterProvider.otherwise('/list')
  // $urlRouterProvider.when('/', '/list')
});
