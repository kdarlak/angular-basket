'use strict';

angular.module('basket').controller('ListController', function($scope, $resource, Basket) {

  var ProductsResouce = $resource('http://localhost:9001/products', {}, {});

  $scope.products = [];

  ProductsResouce.query(function(resp) {
    $scope.products = resp;
  });

  $scope.add = function(product) {
    $scope.basket.addProduct(product);
  };

});
