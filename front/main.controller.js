'use strict';

angular.module('basket').controller('MainController', function($scope, $resource, Basket) {

  var myBasket = new Basket();
  $scope.basket = myBasket;

});
