angular.module('basket')
  .factory('Basket', function(_) {

    function Basket() {
      this.products = [];
      this.uniqueProductsIds = [];
    };

    Basket.prototype.addProduct = function(newProduct) {
      if (_.contains(this.uniqueProductsIds, newProduct.sku)) {
        var addedProduct = _.find(this.products, function(product) {
          return product.sku === newProduct.sku;
        });
        addedProduct.quantity = addedProduct.quantity + 1;
      } else {
        newProduct.quantity = 1;
        this.products.push(newProduct);
        this.uniqueProductsIds.push(newProduct.sku);
      }
    };

    Basket.prototype.getBasketSize = function() {
      var amount = 0;
      angular.forEach(this.products, function(product) {
        amount = amount + product.quantity;
      });

      return amount;
    }

    Basket.prototype.getDiscountValue = function(discount) {
      if (discount !== null) {
        console.log('aaa', discount);
        if (discount.discounttype === "percent") {
          var discountValue = this.getTotalPrice() * (parseInt(discount.amount, 10) / 100);
          return Math.round(discountValue * 100) / 100;
        }
      }

      return 0;
    };

    Basket.prototype.getTotalPrice = function() {
      var amount = 0;
      angular.forEach(this.products, function(product) {
        amount = amount + (product.quantity * product.price);
      });

      return amount;
    }

    return Basket;

});
